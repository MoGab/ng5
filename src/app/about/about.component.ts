import { Component, OnInit ,Input} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router'; 
import { DataService } from '../data.service';
@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements OnInit {

  public ratingValue : number ;
  goals: any;
  constructor(private route: ActivatedRoute,private router: Router, private _data: DataService) { 
    this.route.params.subscribe(res =>  this.ratingValue = res.id);
    //alert(this.ratingValue);
  }

    
  @Input()
  set rating(value : number) {
      this.ratingValue = value;
  } 

  ngOnInit() {
    this._data.goal.subscribe(res => this.goals = res);
  }

  getValueFromParam(id){
    alert(id)
  }

  sendMeHome() {
    this.router.navigate(['']);
  }

}

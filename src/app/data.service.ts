import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
@Injectable()// means we can import it into other components and access its properties and methods
export class DataService {

  private goals = new BehaviorSubject<any>(['The initial goal', 'Another silly life goal']);
  goal = this.goals.asObservable();//property as an observable
  constructor() { }

  //update the goals property
  changeGoal(goal) {
    this.goals.next(goal);
  }
}
